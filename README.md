# docker-gitbucket
GitBucketのdocker構成

## Description
[takezoe/gitbucket](https://hub.docker.com/r/takezoe/gitbucket/)を使用したdocker-compose。  
GitHubクローンのGitBucketを起動する。

## Requirement
- [docker-reverse-proxy](https://gitlab.com/ys_sakai/docker-reverse-proxy)

## Usage
```
root/
　├ gitbucket-data/
　└ docker-compose.yml
```

## 参考
- [[Docker] GitBucketを独自ドメインでさくっと立ち上げる](https://revdev.work/c0d3man/posts/lpx1abxtekswexo9zipw)
